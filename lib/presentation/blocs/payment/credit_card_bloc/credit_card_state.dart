part of 'credit_card_bloc.dart';

abstract class CreditCardState extends Equatable {
  const CreditCardState();
}

class CreditCardInitial extends CreditCardState {
  @override
  List<Object> get props => [];
}
