import 'dart:async';

import 'package:aqbooking/domain/entities/account.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:aqbooking/domain/use_cases/account/index.dart';

part 'account_event.dart';
part 'account_state.dart';

class AccountBloc extends Bloc<AccountEvent, AccountState> {
  final GetAccountInfo getInfo;

  AccountBloc({this.getInfo}) : super(AccountInitial());

  @override
  Stream<AccountState> mapEventToState(AccountEvent event) async* {
    if (event is GetUserInfo) {
      final params = GetAccountParams(token: event.token, userId: event.userId);

      yield await getInfo(params).then(
        (result) => result.fold(
          (info) => AccountLoaded(account: info),
          (failure) => AccountError(),
        ),
      );
    }
  }
}
