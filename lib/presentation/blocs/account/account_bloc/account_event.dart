part of 'account_bloc.dart';

abstract class AccountEvent extends Equatable {
  const AccountEvent();
}

class GetUserInfo extends AccountEvent {
  final String userId;
  final String token;

  const GetUserInfo({@required this.userId, @required this.token});

  @override
  List<Object> get props => [userId, token];
}
