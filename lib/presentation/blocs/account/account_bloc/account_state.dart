part of 'account_bloc.dart';

abstract class AccountState extends Equatable {
  const AccountState();
}

class AccountInitial extends AccountState {
  @override
  List<Object> get props => [];
}

class AccountLoading extends AccountState {
  @override
  List<Object> get props => [];
}

class AccountLoaded extends AccountState {
  final Account account;

  AccountLoaded({this.account});

  @override
  List<Object> get props => [account];
}

class AccountError extends AccountState {
  @override
  List<Object> get props => [];
}
