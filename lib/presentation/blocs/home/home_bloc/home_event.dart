part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class UpdateTabIndex extends HomeEvent {
  final int index;

  UpdateTabIndex({this.index});

  @override
  List<Object> get props => [index];
}

class UpdateServiceIndex extends HomeEvent {
  final int index;

  UpdateServiceIndex({this.index});

  @override
  List<Object> get props => [index];
}

class GetDestinations extends HomeEvent {
  List<Object> get props => [];
}
