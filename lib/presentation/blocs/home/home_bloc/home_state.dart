part of 'home_bloc.dart';

class HomeState extends Equatable {
  final int selectedService;
  final int selectedTab;
  final List<Destination> destinations;

  const HomeState({
    this.selectedService = 0,
    this.selectedTab = 0,
    this.destinations = const [],
  });

  HomeState copyWith({
    int selectedService,
    int currentTab,
    List<Destination> destinations,
  }) =>
      HomeState(
        selectedService: selectedService ?? this.selectedService,
        selectedTab: currentTab ?? this.selectedTab,
        destinations: destinations ?? this.destinations,
      );

  @override
  List<Object> get props => [selectedService, selectedTab, destinations];

  @override
  String toString() =>
      'HomeState { selectedService: $selectedService, selectedTab: $selectedTab, destinations: $destinations }';
}
