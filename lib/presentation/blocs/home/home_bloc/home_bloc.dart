import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/domain/use_cases/destination/index.dart';
import 'package:aqbooking/domain/use_cases/use_case.dart';
import 'package:aqbooking/di/injection_container.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final GetTopDestinations getTopDestinations = sl<GetTopDestinations>();

  HomeBloc() : super(HomeState());

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is UpdateTabIndex) {
      yield state.copyWith(currentTab: event.index);
    } else if (event is UpdateServiceIndex) {
      yield state.copyWith(selectedService: event.index);
    } else if (event is GetDestinations) {
      final destinations = await getTopDestinations(NoParams()).then(
        (result) => result.fold(
          (_destinations) => _destinations,
          (r) => <Destination>[],
        ),
      );

      yield state.copyWith(destinations: destinations);
    }
  }
}
