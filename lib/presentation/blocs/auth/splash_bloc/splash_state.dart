part of 'splash_bloc.dart';

abstract class SplashState extends Equatable {
  const SplashState();

  @override
  List<Object> get props => [];
}

class SplashInitial extends SplashState {}

class SplashChecking extends SplashState {}

class SplashLogged extends SplashState {}

class SplashNotLoggedIn extends SplashState {}
