import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:aqbooking/domain/use_cases/auth/is_logged_in.dart';
import 'package:aqbooking/domain/use_cases/use_case.dart';
import 'package:aqbooking/di/injection_container.dart';

part 'splash_event.dart';
part 'splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final IsLoggedIn isLoggedIn = sl<IsLoggedIn>();

  SplashBloc() : super(SplashInitial());

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    if (event is LetsCheck) {
      yield SplashChecking();

      final failureOrLogged = await isLoggedIn(NoParams());

      yield failureOrLogged.fold(
        (isLogged) => isLogged ? SplashLogged() : SplashNotLoggedIn(),
        (failure) => SplashNotLoggedIn(),
      );
    }
  }
}
