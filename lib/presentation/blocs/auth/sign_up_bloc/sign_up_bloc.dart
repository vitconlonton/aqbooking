import 'dart:async';

import 'package:aqbooking/domain/use_cases/auth/sign_up.dart';
import 'package:aqbooking/di/injection_container.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'sign_up_event.dart';
part 'sign_up_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  final SignUp signUp = sl<SignUp>();

  SignUpBloc() : super(SignUpInitial());

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    if (event is SignUpButtonPressed) {
      yield SignUpProgress();

      final failureOrAuth = await signUp(
        SignUpParams(
          email: event.email,
          phone: event.phone,
          cPassword: event.cPassword,
          password: event.password,
        ),
      );

      yield failureOrAuth.fold(
        (success) => SignUpSuccess(),
        (failure) => SignUpFailure(error: SERVER_FAILURE_MESSAGE),
      );
    }
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
