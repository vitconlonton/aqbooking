part of 'sign_up_bloc.dart';

abstract class SignUpEvent extends Equatable {
  const SignUpEvent();
}

class SignUpButtonPressed extends SignUpEvent {
  final String email;
  final String phone;
  final String password;
  final String cPassword;

  const SignUpButtonPressed({
    @required this.email,
    @required this.phone,
    @required this.password,
    @required this.cPassword,
  });

  @override
  List<Object> get props => [email, phone, password, cPassword];

  @override
  String toString() =>
      'SignUpButtonPressed { email: $email, password: $password }';
}
