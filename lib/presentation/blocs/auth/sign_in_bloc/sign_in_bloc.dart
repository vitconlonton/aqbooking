import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'package:aqbooking/di/injection_container.dart';
import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/domain/use_cases/auth/sign_in.dart';
import 'package:aqbooking/domain/use_cases/auth/sign_in_with_fb.dart';
import 'package:aqbooking/domain/use_cases/use_case.dart';

part 'sign_in_event.dart';
part 'sign_in_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  final SignIn signIn = sl<SignIn>();
  final SignInWithFB signInWithFB = sl<SignInWithFB>();

  SignInBloc() : super(SignInInitial());

  @override
  Stream<SignInState> mapEventToState(SignInEvent event) async* {
    if (event is LoginWithEmail) {
      yield* _handleLoginEmail(event);
    } else if (event is LoginWithFB) {
      yield* _handleLoginFB();
    }
  }

  // Case login with email
  Stream<SignInState> _handleLoginFB() async* {
    yield SignInProgress();

    final failureOrAuth = await signInWithFB(NoParams());

    yield failureOrAuth.fold(
      (credential) => SignInSuccess(credential: credential),
      (failure) => SignInFailure(error: SERVER_FAILURE_MESSAGE),
    );
  }

  // Case login with facebook
  Stream<SignInState> _handleLoginEmail(LoginWithEmail event) async* {
    yield SignInProgress();

    final params = SignInParams(email: event.email, password: event.password);
    final failureOrAuth = await signIn(params);

    yield failureOrAuth.fold(
      (credential) => SignInSuccess(credential: credential),
      (failure) => SignInFailure(error: SERVER_FAILURE_MESSAGE),
    );
  }
}
