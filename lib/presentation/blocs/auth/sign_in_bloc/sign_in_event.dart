part of 'sign_in_bloc.dart';

abstract class SignInEvent extends Equatable {
  const SignInEvent();
}

class LoginWithEmail extends SignInEvent {
  final String email;
  final String password;

  const LoginWithEmail({@required this.email, @required this.password});

  @override
  List<Object> get props => [email, password];

  @override
  String toString() => 'LoginWithEmail { email: $email, password: $password }';
}

class LoginWithFB extends SignInEvent {
  const LoginWithFB();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'LoginWithFB { }';
}
