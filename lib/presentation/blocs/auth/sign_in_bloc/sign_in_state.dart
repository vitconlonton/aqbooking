part of 'sign_in_bloc.dart';

abstract class SignInState extends Equatable {}

class SignInInitial extends SignInState {
  @override
  List<Object> get props => [];

  String toString() => 'SignInInitial { }';
}

class SignInProgress extends SignInState {
  @override
  List<Object> get props => [];

  String toString() => 'SignInProgress { }';
}

class SignInSuccess extends SignInState {
  final Credential credential;

  SignInSuccess({@required this.credential});

  @override
  List<Object> get props => [credential];

  String toString() => 'SignInSuccess { error: $credential }';
}

class SignInFailure extends SignInState {
  final String error;

  SignInFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SignInFailure { error: $error }';
}
