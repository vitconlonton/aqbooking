import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/domain/use_cases/activity/index.dart';
import 'package:aqbooking/di/injection_container.dart';

part 'destination_event.dart';
part 'destination_state.dart';

class DestinationBloc extends Bloc<DestinationEvent, DestinationState> {
  final GetActivitiesByDesId getActivitiesByDesId = sl<GetActivitiesByDesId>();

  DestinationBloc()
      : super(
          DestinationState(
            destination: null,
            activities: const [],
            status: DestinationStatus.init,
          ),
        );

  @override
  Stream<DestinationState> mapEventToState(DestinationEvent event) async* {
    if (event is GetActivities) {
      // Update state to loading
      yield state.copyWith(
        destination: event.destination,
        status: DestinationStatus.loading,
      );

      // Set up params
      final params = GetActivitiesByDesIdParams(desId: 1);

      // Fetch and update state if success or failure
      yield await getActivitiesByDesId(params)
          .catchError((e) => state.copyWith(status: DestinationStatus.error))
          .then(
            (activitiesOrFailure) => activitiesOrFailure.fold(
              (activities) => state.copyWith(
                activities: activities,
                status: DestinationStatus.done,
              ),
              (failure) => state.copyWith(status: DestinationStatus.error),
            ),
          );
    }
  }
}
