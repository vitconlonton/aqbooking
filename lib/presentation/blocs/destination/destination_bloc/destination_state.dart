part of 'destination_bloc.dart';

enum DestinationStatus { init, loading, error, done }

class DestinationState extends Equatable {
  final Destination destination;
  final List<Activity> activities;
  final DestinationStatus status;

  DestinationState({this.destination, this.activities, this.status});

  DestinationState copyWith({
    Destination destination,
    List<Activity> activities,
    DestinationStatus status,
  }) =>
      DestinationState(
        destination: destination ?? this.destination,
        activities: activities ?? this.activities,
        status: status ?? this.status,
      );

  @override
  List<Object> get props => [destination, activities, status];

  @override
  String toString() =>
      'DestinationState { destination: $destination, activities: $activities, status: $status }';
}
