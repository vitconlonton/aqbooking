part of 'destination_bloc.dart';

abstract class DestinationEvent extends Equatable {
  const DestinationEvent();
}

class GetActivities extends DestinationEvent {
  final Destination destination;

  GetActivities({@required this.destination}) : assert(destination != null);

  @override
  List<Object> get props => [];
}
