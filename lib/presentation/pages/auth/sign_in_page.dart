import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:aqbooking/di/injection_container.dart';
import 'package:aqbooking/routes.dart';
import 'package:aqbooking/presentation/stores/app_bloc/app_bloc.dart';
import 'package:aqbooking/presentation/widgets/auth/index.dart';
import 'package:aqbooking/presentation/widgets/common/index.dart';
import 'package:aqbooking/presentation/blocs/auth/sign_in_bloc/sign_in_bloc.dart';

class SignInPage extends StatefulWidget {
  final String title;

  SignInPage({Key key, this.title}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  // Bloc
  final SignInBloc _signInBloc = SignInBloc();
  AppBloc _appBloc = sl<AppBloc>();

  // Text controllers
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    // Set default values
    _emailController.text = 'eve.holt@reqres.in';
    _passwordController.text = 'pistol';

    // Listen bloc state
    _signInBloc.listen(_onStateChange);
    _appBloc
        .any((state) => state.isAuthenticated)
        .then((value) => value ? _goToHome() : null);
  }

  /// Life cycle
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            BezierBackground(),
            SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: height * .2),
                  AuthTitle(),
                  SizedBox(height: 50),
                  TextInput(
                    title: "Username",
                    controller: _emailController,
                  ),
                  TextInput(
                    title: "Password",
                    obscureText: true,
                    controller: _passwordController,
                  ),
                  SizedBox(height: 20),
                  SubmitButton(
                    title: "Sign in",
                    onTap: () => _loginEmail(),
                  ),
                  SizedBox(height: 20),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.center,
                    child: Text('Forgot Password ?'),
                  ),
                  AuthDivider(),
                  FacebookButton(onTap: () => _loginFB()),
                  SizedBox(height: height * .055),
                  TextLink(
                    title: 'Don\'t have an account? ',
                    subTitle: 'Register',
                    onTap: () => _goToRegister(),
                  )
                ],
              ),
            ),
            BlocBuilder<SignInBloc, SignInState>(
              bloc: _signInBloc,
              builder: (context, signInState) => (signInState is SignInProgress)
                  ? LoadingDialog()
                  : Container(),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Future<void> dispose() async {
    super.dispose();

    // Close streams
    _emailController.dispose();
    _passwordController.dispose();
    _signInBloc.close();
  }

  // Login with facebook
  void _loginFB() => _signInBloc.add(LoginWithFB());

  // Login with facebook
  void _goToRegister() => Navigator.popAndPushNamed(context, Routes.sign_up);

  // Login with facebook
  void _goToHome() => Navigator.popAndPushNamed(context, Routes.home);

  // Emit to bloc state change
  void _onStateChange(SignInState state) {
    if (state is SignInSuccess) {
      _appBloc.add(UpdateCredential(credential: state.credential));
    } else if (state is SignInFailure) {
      _showMessage(state.error);
    }
  }

  // Show message
  void _showMessage(String message) {
    SnackBar snackBar = AQSnackBar(
      message: message,
      action: SnackBarAction(
        label: 'Close',
        onPressed: null,
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  // Submit login
  void _loginEmail() => _signInBloc.add(
        LoginWithEmail(
          email: _emailController.value.text,
          password: _passwordController.value.text,
        ),
      );
}
