import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:aqbooking/routes.dart';
import 'package:aqbooking/presentation/blocs/auth/sign_up_bloc/sign_up_bloc.dart';
import 'package:aqbooking/presentation/widgets/auth/index.dart';
import 'package:aqbooking/presentation/widgets/common/index.dart';

class SignUpPage extends StatefulWidget {
  final String title;

  SignUpPage({Key key, this.title}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  SignUpBloc _bloc = new SignUpBloc();

  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();
  final _cPasswordController = TextEditingController();

  void _goBack() => Navigator.of(context).pop();

  void _goToHome() => Navigator.of(context).pushNamedAndRemoveUntil(
        Routes.home,
        (route) => true,
      );

  void _submit() => _bloc.add(
        SignUpButtonPressed(
          email: _emailController.value.text,
          phone: _phoneController.value.text,
          password: _passwordController.value.text,
          cPassword: _cPasswordController.value.text,
        ),
      );

  @override
  void initState() {
    super.initState();

    _emailController.text = 'eve.holt@reqres.in';
    _phoneController.text = '0372549837';
    _cPasswordController.text = 'pistol';
    _passwordController.text = 'pistol';

    _bloc.any((state) => state is SignUpSuccess).then((value) => _goToHome());
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return BlocBuilder(
      bloc: _bloc,
      builder: (_, state) {
        return Scaffold(
          body: Container(
            child: Stack(
              children: [
                BezierBackground(),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(height: height * .12),
                        AuthTitle(),
                        SizedBox(height: 20),
                        TextInput(title: "Email", controller: _emailController),
                        TextInput(title: "Phone", controller: _phoneController),
                        TextInput(
                          title: "Confirm Password",
                          obscureText: true,
                          controller: _cPasswordController,
                        ),
                        TextInput(
                          title: "Password",
                          obscureText: true,
                          controller: _passwordController,
                        ),
                        SizedBox(height: 20),
                        SubmitButton(
                          title: 'Register Now',
                          onTap: () => _submit(),
                        ),
                        SizedBox(height: height * .10),
                        TextLink(
                            title: 'Already have an account? ',
                            subTitle: 'Login',
                            onTap: () => _goBack()),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 40,
                  child: BackButton(onPressed: () => _goBack()),
                ),
                (state is SignUpProgress) ? LoadingDialog() : Container()
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _phoneController.dispose();
    _cPasswordController.dispose();
    _passwordController.dispose();
    _bloc.close();
    super.dispose();
  }
}
