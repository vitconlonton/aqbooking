import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:aqbooking/routes.dart';
import 'package:aqbooking/presentation/blocs/auth/splash_bloc/splash_bloc.dart';
import 'package:aqbooking/presentation/widgets/common/index.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  SplashBloc _bloc = new SplashBloc();

  void _goTo(String route) => Future.delayed(
        Duration(milliseconds: 500),
      ).then((value) => Navigator.popAndPushNamed(context, route));

  void _onSplashStateChange(SplashState state) {
    if (state is SplashInitial)
      _bloc.add(LetsCheck());
    else if (state is SplashLogged)
      _goTo(Routes.home);
    else if (state is SplashNotLoggedIn) _goTo(Routes.sign_in);
  }

  @override
  void initState() {
    super.initState();

    // Listen bloc state
    _bloc.listen(_onSplashStateChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: _bloc,
      builder: (_, state) => Scaffold(body: Center(child: LoadingDialog())),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _bloc.close();
  }
}
