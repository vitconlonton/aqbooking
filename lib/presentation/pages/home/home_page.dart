import 'package:aqbooking/presentation/widgets/account/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:aqbooking/di/injection_container.dart';
import 'package:aqbooking/routes.dart';
import 'package:aqbooking/presentation/blocs/home/home_bloc/home_bloc.dart';
import 'package:aqbooking/presentation/stores/app_bloc/app_bloc.dart';
import 'package:aqbooking/presentation/widgets/destination/index.dart';
import 'package:aqbooking/presentation/widgets/home/index.dart';
import 'package:aqbooking/presentation/pages/destination/destination_page.dart';
import 'package:aqbooking/presentation/widgets/common/index.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeBloc _homeBloc = HomeBloc();
  AppBloc _appBloc = sl();

  @override
  void initState() {
    super.initState();

    _appBloc.add(GetAccount());
    _homeBloc.add(GetDestinations());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            ListView(
              padding: EdgeInsets.symmetric(vertical: 30.0),
              children: [
                HomeTitle(),

                SizedBox(height: 20.0),
                BlocBuilder<HomeBloc, HomeState>(
                  bloc: _homeBloc,
                  buildWhen: (prev, cur) {
                    return prev.selectedService != cur.selectedService;
                  },
                  builder: (context, state) {
                    return IconsService(
                      selectedIndex: state.selectedService,
                      onTap: (index) {
                        _homeBloc.add(UpdateServiceIndex(index: index));
                      },
                    );
                  },
                ),

                BlocBuilder<HomeBloc, HomeState>(
                  bloc: _homeBloc,
                  builder: (context, state) {
                    return SearchInput(
                      onTap: () => showSearch(
                        context: context,
                        delegate: SearchDestinationDelegate(),
                      ).then((value) {
                        if (value == null) return;

                        var destination = state.destinations.firstWhere(
                          (element) => element.city == value,
                          orElse: () => state.destinations[0],
                        );

                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) =>
                                DestinationPage(destination: destination),
                          ),
                        );
                      }),
                    );
                  },
                ),

                SizedBox(height: 20.0),

                BlocBuilder<HomeBloc, HomeState>(
                  bloc: _homeBloc,
                  buildWhen: (prev, cur) =>
                      prev.destinations != cur.destinations,
                  builder: (context, state) {
                    return DestinationCarousel(
                      onItemTap: (destination) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => DestinationPage(
                              destination: destination,
                            ),
                          ),
                        );
                      },
                      destinations: state.destinations,
                    );
                  },
                ),
                SizedBox(height: 20.0),

                BlocBuilder<AppBloc, AppState>(
                  bloc: _appBloc,
                  builder: (context, appState) {
                    return BlocConsumer<HomeBloc, HomeState>(
                      bloc: _homeBloc,
                      listenWhen: (prev, cur) => cur.selectedTab == 2,
                      buildWhen: (prev, cur) =>
                          cur.selectedTab != prev.selectedTab,
                      listener: (context, state) => _appBloc.add(AppSignOut()),
                      builder: (context, state) {
                        var imgUri = 'http://i.imgur.com/zL4Krbz.jpg';
                        var name = "";
                        var id = "";
                        final account = _appBloc.state.account;

                        if (account != null) {
                          imgUri = account.picture ?? imgUri;
                          name = account.name ?? name;
                          id = account.id ?? id;
                        }

                        return Container(
                          child: FittedBox(
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ProfileTile(title: name, subtitle: id),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    ProfileImage(uri: imgUri),
                                    IconButton(
                                      iconSize: 10,
                                      icon: Icon(Icons.exit_to_app),
                                      color: Colors.black,
                                      onPressed: () => _appBloc.add(
                                        AppSignOut(),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  },
                ),

                // HotelCarousel(),
              ],
            ),
            BlocConsumer<AppBloc, AppState>(
              bloc: _appBloc,
              buildWhen: (prev, cur) => prev.status != cur.status,
              listenWhen: (prev, cur) => !cur.isAuthenticated,
              listener: (context, appState) {
                Navigator.of(context).pushNamedAndRemoveUntil(
                  Routes.sign_in,
                  (route) => false,
                );
              },
              builder: (context, appState) {
                if (appState.status == AppStatus.loading) {
                  return LoadingDialog();
                }

                return Container();
              },
            )
          ],
        ),
      ),
      bottomNavigationBar: BlocBuilder<AppBloc, AppState>(
        bloc: _appBloc,
        builder: (context, appState) {
          return BlocConsumer<HomeBloc, HomeState>(
            bloc: _homeBloc,
            listenWhen: (prev, cur) => cur.selectedTab == 2,
            buildWhen: (prev, cur) => cur.selectedTab != prev.selectedTab,
            listener: (context, state) => _appBloc.add(AppSignOut()),
            builder: (context, state) {
              var imgUri = 'http://i.imgur.com/zL4Krbz.jpg';
              final account = _appBloc.state.account;

              if (account != null && account.picture != null) {
                imgUri = account.picture;
              }

              return HomeBottomNavigationBar(
                imgUri: imgUri,
                currentIndex: state.selectedTab,
                onTap: (index) => _homeBloc.add(
                  UpdateTabIndex(index: index),
                ),
              );
            },
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _homeBloc.close();
  }
}
