import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:aqbooking/routes.dart';
import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/presentation/blocs/destination/destination_bloc/destination_bloc.dart';
import 'package:aqbooking/presentation/widgets/activity/index.dart';
import 'package:aqbooking/presentation/widgets/destination/index.dart';
import 'package:aqbooking/presentation/widgets/common/index.dart';

class DestinationPage extends StatefulWidget {
  final Destination destination;

  DestinationPage({@required this.destination});

  @override
  _DestinationPageState createState() => _DestinationPageState();
}

class _DestinationPageState extends State<DestinationPage> {
  final DestinationBloc _bloc = DestinationBloc();

  @override
  void initState() {
    super.initState();

    _bloc.add(GetActivities(destination: widget.destination));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          offset: Offset(0.0, 2.0),
                          blurRadius: 6.0,
                        ),
                      ],
                    ),
                    child: Hero(
                      tag: widget.destination.imageUrl ?? "",
                      child: DestinationImage(
                        width: MediaQuery.of(context).size.width,
                        uri: widget.destination.imageUrl,
                        borderRadius: 30.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 10.0,
                      vertical: 40.0,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        BackButton(onPressed: () => Navigator.pop(context))
                      ],
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    bottom: 20.0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        DestinationCity(
                          widget.destination.city,
                          textStyle: TextStyle(fontSize: 35.0),
                        ),
                        Row(
                          children: [
                            Icon(
                              FontAwesomeIcons.locationArrow,
                              size: 15.0,
                              color: Colors.white70,
                            ),
                            SizedBox(width: 5.0),
                            DestinationCountry(
                              widget.destination.country,
                              textStyle: TextStyle(
                                color: Colors.white70,
                                fontSize: 20.0,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 20.0,
                    bottom: 20.0,
                    child: Icon(
                      Icons.location_on,
                      color: Colors.white70,
                      size: 25.0,
                    ),
                  ),
                ],
              ),
              Expanded(
                child: BlocBuilder<DestinationBloc, DestinationState>(
                  bloc: _bloc,
                  builder: (context, state) {
                    return ListView.builder(
                      padding: EdgeInsets.only(top: 10.0, bottom: 15.0),
                      itemCount: state.activities.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ActivityItem(
                          activity: state.activities[index],
                          onTap: (activity) => Navigator.of(context).pushNamed(
                            Routes.payment,
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
            ],
          ),
          BlocBuilder<DestinationBloc, DestinationState>(
            bloc: _bloc,
            buildWhen: (prev, cur) => prev.status != cur.status,
            builder: (context, state) {
              // If status show loading
              if (state.status == DestinationStatus.loading) {
                return LoadingDialog();
              }

              return Container();
            },
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();

    _bloc.close();
  }
}
