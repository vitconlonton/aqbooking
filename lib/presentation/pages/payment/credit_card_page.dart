import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

import 'package:aqbooking/routes.dart';
import 'package:aqbooking/presentation/blocs/payment/credit_card_bloc/credit_card_bloc.dart';
import 'package:aqbooking/presentation/widgets/payment/index.dart';

class CreditCardPage extends StatefulWidget {
  @override
  _CreditCardPageState createState() => _CreditCardPageState();
}

class _CreditCardPageState extends State<CreditCardPage> {
  final _creditBloc = CreditCardBloc();

  MaskedTextController expMask = MaskedTextController(mask: "00/00");
  MaskedTextController ccMask = MaskedTextController(
    mask: "0000 0000 0000 0000",
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(centerTitle: false, title: Text("Credit Card")),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CreditCard(
              ccMask: ccMask,
              expMask: expMask,
              ccOutputStream: _creditBloc.ccOutputStream,
              cvvOutputStream: _creditBloc.cvvOutputStream,
              expOutputStream: _creditBloc.expOutputStream,
              nameOutputStream: _creditBloc.nameOutputStream,
            ),
            FillEntries(
              ccMask: ccMask,
              expMask: expMask,
              ccInputSink: _creditBloc.ccInputSink,
              expInputSink: _creditBloc.expInputSink,
              cvvInputSink: _creditBloc.cvvInputSink,
              nameInputSink: _creditBloc.nameInputSink,
            )
          ],
        ),
      ),
      floatingActionButton: FloatingBar(
        onPress: () => Navigator.of(context).pushNamed(Routes.payment_success),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _creditBloc?.close();
  }
}
