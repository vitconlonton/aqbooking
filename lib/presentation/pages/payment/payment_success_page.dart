import 'package:aqbooking/routes.dart';
import 'package:flutter/material.dart';

import 'package:aqbooking/presentation/widgets/payment/index.dart';
import 'package:aqbooking/presentation/widgets/common/index.dart';

class PaymentSuccessPage extends StatefulWidget {
  @override
  PaymentSuccessPageState createState() => PaymentSuccessPageState();
}

class PaymentSuccessPageState extends State<PaymentSuccessPage> {
  bool isDataAvailable = true;

  @override
  Widget build(BuildContext context) {
    return CommonScaffold(
      appTitle: "Payment Success",
      actionFirstIcon: null,
      bodyData: Center(
        child: isDataAvailable
            ? RaisedButton(
                shape: StadiumBorder(),
                onPressed: () => showSuccessDialog(),
                color: Colors.amber,
                child: Text("Process Payment"),
              )
            : CircularProgressIndicator(),
      ),
    );
  }

  void showSuccessDialog() {
    setState(() {
      isDataAvailable = false;
      Future.delayed(Duration(seconds: 3)).then((_) => goToDialog());
    });
  }

  goToDialog() {
    setState(() {
      isDataAvailable = true;
    });
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SuccessTicket(),
                    SizedBox(height: 10.0),
                    FloatingActionButton(
                      backgroundColor: Colors.black,
                      child: Icon(Icons.clear, color: Colors.white),
                      onPressed: () => Navigator.of(
                        context,
                      ).pushNamedAndRemoveUntil(Routes.home, (route) => false),
                    )
                  ],
                ),
              ),
            ));
  }
}
