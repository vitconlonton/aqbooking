part of 'app_bloc.dart';

enum AppStatus { loading, loaded, error }

class AppState extends Equatable {
  final Credential credential;
  final Account account;
  final bool isAuthenticated;
  final AppStatus status;

  const AppState({
    @required this.credential,
    @required this.account,
    this.isAuthenticated = false,
    this.status = AppStatus.loaded,
  });

  AppState copyWith({
    Credential credential,
    Account account,
    bool isAuthenticated,
    AppStatus status,
  }) =>
      AppState(
        credential: credential ?? this.credential,
        account: account ?? this.account,
        isAuthenticated: isAuthenticated ?? this.isAuthenticated,
        status: status ?? this.status,
      );

  @override
  List<Object> get props => [credential, account, isAuthenticated, status];

  @override
  String toString() =>
      'AppState { credential: $credential, account: $account, isAuthenticated: $isAuthenticated, status: $status }';
}
