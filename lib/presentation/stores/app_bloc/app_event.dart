part of 'app_bloc.dart';

abstract class AppEvent extends Equatable {
  const AppEvent();
}

class UpdateCredential extends AppEvent {
  final Credential credential;

  UpdateCredential({@required this.credential});

  @override
  List<Object> get props => [credential];
}

class AppSignOut extends AppEvent {
  @override
  List<Object> get props => [];
}

class GetAccount extends AppEvent {
  @override
  List<Object> get props => [];
}
