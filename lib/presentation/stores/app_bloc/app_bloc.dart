import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/domain/use_cases/account/index.dart';
import 'package:aqbooking/domain/use_cases/auth/index.dart';
import 'package:aqbooking/domain/use_cases/use_case.dart';
import 'package:aqbooking/di/injection_container.dart';

part 'app_event.dart';
part 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  final GetAccountInfo getAccountInfo = sl<GetAccountInfo>();
  final SignOut signOut = sl<SignOut>();

  AppBloc()
      : super(
          AppState(status: AppStatus.loaded, account: null, credential: null),
        );

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    if (event is UpdateCredential) {
      yield _mapToUpdateCredential(event);
    } else if (event is AppSignOut) {
      yield* _mapToAppSignOut(event);
    } else if (event is GetAccount) {
      yield* _mapToGetAccount(event);
    }
  }

  AppState _mapToUpdateCredential(UpdateCredential event) {
    return state.copyWith(credential: event.credential, isAuthenticated: true);
  }

  Stream<AppState> _mapToAppSignOut(AppSignOut event) async* {
    yield state.copyWith(status: AppStatus.loading);

    bool isLogout = await signOut(NoParams()).then(
      (successOrFailure) => successOrFailure.fold(
        (value) => value,
        (failure) => false,
      ),
    );

    yield state.copyWith(
      account: null,
      credential: null,
      isAuthenticated: !isLogout,
      status: isLogout ? AppStatus.loaded : AppStatus.error,
    );
  }

  Stream<AppState> _mapToGetAccount(GetAccount event) async* {
    final userId = state.credential.userId;
    final token = state.credential.accessToken;

    final params = GetAccountParams(userId: userId, token: token);

    final account = await getAccountInfo(params)
        .then((result) => result.fold((info) => info, (failure) => null))
        .then((info) => info);

    final status = account != null ? AppStatus.loaded : AppStatus.error;

    yield state.copyWith(account: account, status: status);
  }
}
