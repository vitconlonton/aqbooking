import 'package:flutter/material.dart';

class SearchInput extends StatelessWidget {
  final _textStyle = TextStyle(fontSize: 15.0, color: Colors.blueGrey[300]);

  final Function onTap;

  SearchInput({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.blueGrey[50],
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        child: TextField(
          style: _textStyle.copyWith(fontSize: 15.0),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(10.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
              borderSide: BorderSide(color: Colors.white),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(5.0),
            ),
            hintText: "E.g: New York, United States",
            prefixIcon: Icon(Icons.location_on, color: Colors.blueGrey[300]),
            hintStyle: _textStyle,
          ),
          onTap: onTap,
          maxLines: 1,
        ),
      ),
    );
  }
}
