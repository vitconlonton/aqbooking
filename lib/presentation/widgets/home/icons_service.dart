import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class IconsService extends StatefulWidget {
  final int selectedIndex;
  final Function(int value) onTap;

  const IconsService(
      {Key key, @required this.selectedIndex, @required this.onTap})
      : super(key: key);

  @override
  _IconsServiceState createState() => _IconsServiceState();
}

class _IconsServiceState extends State<IconsService> {
  List<IconData> _icons = [
    FontAwesomeIcons.plane,
    FontAwesomeIcons.bed,
    FontAwesomeIcons.walking,
    FontAwesomeIcons.biking,
  ];

  Widget _buildIcon(int index, BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onTap(index);
      },
      child: Container(
        height: 60.0,
        width: 60.0,
        decoration: BoxDecoration(
          color: widget.selectedIndex == index
              ? Theme.of(context).accentColor
              : Color(0xFFE7EBEE),
          borderRadius: BorderRadius.circular(30.0),
        ),
        child: Icon(
          _icons[index],
          size: 25.0,
          color: widget.selectedIndex == index
              ? Theme.of(context).buttonColor
              : Color(0xFFB4C1C4),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: _icons
          .asMap()
          .entries
          .map((MapEntry map) => _buildIcon(map.key, context))
          .toList(),
    );
  }
}
