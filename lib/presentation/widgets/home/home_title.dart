import 'package:flutter/material.dart';

class HomeTitle extends StatelessWidget {
  final _style = TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold);

  HomeTitle({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20.0, right: 120.0),
      child: Text('What would you like to find?', style: _style),
    );
  }
}
