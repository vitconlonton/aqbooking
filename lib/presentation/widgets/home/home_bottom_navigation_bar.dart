import 'package:flutter/material.dart';

class HomeBottomNavigationBar extends StatelessWidget {
  static const Widget _title = SizedBox.shrink();

  final String imgUri;
  final int currentIndex;
  final Function(int value) onTap;

  HomeBottomNavigationBar({
    Key key,
    @required this.currentIndex,
    @required this.onTap,
    this.imgUri,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: currentIndex,
      onTap: onTap,
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.search, size: 30.0), title: _title),
        BottomNavigationBarItem(
            icon: Icon(Icons.favorite_border, size: 30.0), title: _title),
        BottomNavigationBarItem(
          icon:
              CircleAvatar(radius: 15.0, backgroundImage: NetworkImage(imgUri)),
          title: _title,
        )
      ],
    );
  }
}
