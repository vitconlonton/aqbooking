import 'package:flutter/material.dart';

class TextInput extends StatelessWidget {
  final title;
  final bool obscureText;
  final TextEditingController controller;

  final _titleStyle = TextStyle(fontWeight: FontWeight.bold, fontSize: 15);
  final _decoration = InputDecoration(
    border: InputBorder.none,
    fillColor: Color(0xfff3f3f4),
    filled: true,
  );

  TextInput({
    Key key,
    this.title,
    this.obscureText = false,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          title != null ? Text(title, style: _titleStyle) : null,
          SizedBox(height: 10),
          TextField(
            obscureText: obscureText,
            controller: controller,
            decoration: _decoration,
          )
        ],
      ));

  // TextInput({this.obscureText, this.controller})
  //     : super(obscureText: obscureText, controller: controller);

  // @override
  // InputDecoration get decoration => InputDecoration(
  //       border: InputBorder.none,
  //       fillColor: Color(0xfff3f3f4),
  //       filled: true,
  //     );
}
