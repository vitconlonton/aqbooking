import 'package:flutter/material.dart';

class CommonDrawer extends StatelessWidget {
  final _textStyle = TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0);

  final String uri;
  final String accountName;
  final String accountEmail;

  CommonDrawer({
    Key key,
    this.uri,
    this.accountName = "Alvin",
    this.accountEmail = "mtechviral@gmail.com",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(accountName),
            accountEmail: Text(accountEmail),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(uri),
            ),
          ),
          ListTile(
            title: Text("Profile", style: _textStyle),
            leading: Icon(Icons.person, color: Colors.blue),
          ),
          ListTile(
            title: Text("Shopping", style: _textStyle),
            leading: Icon(Icons.shopping_cart, color: Colors.green),
          ),
          ListTile(
            title: Text("Dashboard", style: _textStyle),
            leading: Icon(Icons.dashboard, color: Colors.red),
          ),
          ListTile(
            title: Text("Timeline", style: _textStyle),
            leading: Icon(Icons.timeline, color: Colors.cyan),
          ),
          Divider(),
          ListTile(
            title: Text("Settings", style: _textStyle),
            leading: Icon(Icons.settings, color: Colors.brown),
          ),
          Divider(),
          // MyAboutTile()
        ],
      ),
    );
  }
}
