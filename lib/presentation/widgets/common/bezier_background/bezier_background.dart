import 'package:flutter/material.dart';

import 'bezier_container.dart';

class BezierBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Positioned(
        top: -MediaQuery.of(context).size.height * .15,
        right: -MediaQuery.of(context).size.width * .4,
        child: BezierContainer(),
      );
}
