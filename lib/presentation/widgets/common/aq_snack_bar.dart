import 'package:flutter/material.dart';

class AQSnackBar extends SnackBar {
  final String message;
  final SnackBarAction action;

  @override
  AQSnackBar({
    Key key,
    @required this.message,
    this.action,
  }) : super(key: key, content: Text(message), action: action);

  @override
  Duration get duration => Duration(milliseconds: 800);
}
