import 'package:flutter/material.dart';

abstract class Screen extends StatefulWidget {
  final String routeName;

  const Screen({Key key, this.routeName}) : super(key: key);
}
