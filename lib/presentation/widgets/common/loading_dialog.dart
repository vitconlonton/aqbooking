import 'package:flutter/material.dart';

class LoadingDialog extends StatelessWidget {
  const LoadingDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        color: Colors.transparent,
        child: Center(child: CircularProgressIndicator()),
      );
}
