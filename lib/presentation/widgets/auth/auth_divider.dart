import 'package:flutter/material.dart';

class AuthDivider extends StatelessWidget {
  final _divider = Expanded(
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Divider(thickness: 1),
    ),
  );

  final _padding = SizedBox(width: 20, height: 30);

  final _text = Text('or');

  @override
  Widget build(BuildContext context) => Container(
        child: Row(children: [_padding, _divider, _text, _divider, _padding]),
      );
}
