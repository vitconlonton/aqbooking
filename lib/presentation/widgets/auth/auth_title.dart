import 'package:flutter/material.dart';

class AuthTitle extends StatelessWidget {
  final _textStyle = TextStyle(fontSize: 30, color: Color(0xffe46b10));

  @override
  Widget build(BuildContext context) => RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'd',
          style: _textStyle.copyWith(fontWeight: FontWeight.w700),
          children: [
            TextSpan(
              text: 'ev',
              style: _textStyle.copyWith(color: Colors.black),
            ),
            TextSpan(text: 'rnz', style: _textStyle),
          ],
        ),
      );
}
