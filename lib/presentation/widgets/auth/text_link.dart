import 'package:flutter/material.dart';

class TextLink extends StatelessWidget {
  static const _style = TextStyle(fontSize: 13, fontWeight: FontWeight.w600);

  final String title;
  final String subTitle;
  final Function onTap;

  const TextLink({Key key, this.title, this.subTitle, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: onTap,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title, style: _style),
            Text(subTitle, style: _style.copyWith(color: Color(0xfff79c4f))),
          ],
        ),
      ),
    );
  }
}
