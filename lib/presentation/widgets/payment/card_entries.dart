import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

import 'package:aqbooking/presentation/widgets/account/index.dart';

class CardEntries extends StatelessWidget {
  final Stream<String> ccOutputStream;
  final Stream<String> expOutputStream;
  final Stream<String> cvvOutputStream;
  final MaskedTextController ccMask;
  final MaskedTextController expMask;

  const CardEntries({
    Key key,
    @required this.ccOutputStream,
    @required this.expOutputStream,
    @required this.cvvOutputStream,
    @required this.ccMask,
    @required this.expMask,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StreamBuilder<String>(
              stream: ccOutputStream,
              initialData: "**** **** **** ****",
              builder: (context, snapshot) {
                if (snapshot.data.length > 0) {
                  ccMask.updateText(snapshot.data);
                }
                return Text(
                  snapshot.data.length > 0
                      ? snapshot.data
                      : "**** **** **** ****",
                  style: TextStyle(color: Colors.white, fontSize: 22.0),
                );
              }),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              StreamBuilder<String>(
                  stream: expOutputStream,
                  initialData: "MM/YY",
                  builder: (context, snapshot) {
                    if (snapshot.data.length > 0) {
                      expMask.updateText(snapshot.data);
                    }

                    return ProfileTile(
                      textColor: Colors.white,
                      title: "Expiry",
                      subtitle:
                          snapshot.data.length > 0 ? snapshot.data : "MM/YY",
                    );
                  }),
              SizedBox(
                width: 30.0,
              ),
              StreamBuilder<String>(
                  stream: cvvOutputStream,
                  initialData: "***",
                  builder: (context, snapshot) => ProfileTile(
                        textColor: Colors.white,
                        title: "CVV",
                        subtitle:
                            snapshot.data.length > 0 ? snapshot.data : "***",
                      )),
            ],
          ),
        ],
      ),
    );
  }
}
