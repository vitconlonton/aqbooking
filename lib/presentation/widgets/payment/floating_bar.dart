import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:aqbooking/core/util/index.dart';

class FloatingBar extends StatelessWidget {
  final Function onPress;

  const FloatingBar({Key key, this.onPress}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: ShapeDecoration(
        shape: StadiumBorder(),
        gradient: LinearGradient(colors: UIData.kitGradients),
      ),
      child: FloatingActionButton.extended(
        onPressed: onPress,
        backgroundColor: Colors.transparent,
        icon: Icon(FontAwesomeIcons.amazonPay, color: Colors.white),
        label: Text("Continue", style: TextStyle(color: Colors.white)),
      ),
    );
  }
}
