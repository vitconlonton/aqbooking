import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:aqbooking/presentation/widgets/account/index.dart';

class SuccessTicket extends StatelessWidget {
  const SuccessTicket({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(16.0),
      child: Material(
        clipBehavior: Clip.antiAlias,
        elevation: 2.0,
        borderRadius: BorderRadius.circular(4.0),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              ProfileTile(
                title: "Thank You!",
                textColor: Colors.purple,
                subtitle: "Your transaction was successful",
              ),
              ListTile(
                title: Text("Date"),
                subtitle: Text("10 July 2020"),
                trailing: Text("11:00 AM"),
              ),
              ListTile(
                title: Text("Alvin"),
                subtitle: Text("mtechviral@gmail.com"),
                trailing: CircleAvatar(
                  radius: 20.0,
                  backgroundImage: NetworkImage(
                      "https://avatars0.githubusercontent.com/u/12619420?s=460&v=4"),
                ),
              ),
              ListTile(
                title: Text("Amount"),
                subtitle: Text("\$299"),
                trailing: Text("Completed"),
              ),
              Card(
                clipBehavior: Clip.antiAlias,
                elevation: 0.0,
                color: Colors.grey.shade300,
                child: ListTile(
                  leading: Icon(FontAwesomeIcons.ccAmex, color: Colors.blue),
                  title: Text("Credit/Debit Card"),
                  subtitle: Text("Amex Card ending ***6"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
