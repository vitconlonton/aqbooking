import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:aqbooking/core/util/index.dart';

import 'index.dart';

class CreditCard extends StatelessWidget {
  final Stream<String> nameOutputStream;
  final Stream<String> ccOutputStream;
  final Stream<String> expOutputStream;
  final Stream<String> cvvOutputStream;
  final MaskedTextController ccMask;
  final MaskedTextController expMask;

  const CreditCard({
    Key key,
    @required this.nameOutputStream,
    @required this.ccOutputStream,
    @required this.expOutputStream,
    @required this.cvvOutputStream,
    @required this.ccMask,
    @required this.expMask,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final cardEntries = CardEntries(
      ccMask: ccMask,
      expMask: expMask,
      ccOutputStream: ccOutputStream,
      cvvOutputStream: cvvOutputStream,
      expOutputStream: expOutputStream,
    );

    return Container(
      height: deviceSize.height * 0.3,
      color: Colors.grey.shade300,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          clipBehavior: Clip.antiAlias,
          elevation: 3.0,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(colors: UIData.kitGradients),
                ),
              ),
              Opacity(
                opacity: 0.1,
                child: Container(color: Colors.blueGrey[700]),
              ),
              MediaQuery.of(context).orientation == Orientation.portrait
                  ? cardEntries
                  : FittedBox(child: cardEntries),
              Positioned(
                right: 10.0,
                top: 10.0,
                child: Icon(
                  FontAwesomeIcons.ccVisa,
                  size: 30.0,
                  color: Colors.white,
                ),
              ),
              Positioned(
                right: 10.0,
                bottom: 10.0,
                child: StreamBuilder<String>(
                  stream: nameOutputStream,
                  initialData: "Your Name",
                  builder: (context, snapshot) => Text(
                    snapshot.data.length > 0 ? snapshot.data : "Your Name",
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
