import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class FillEntries extends StatelessWidget {
  final Sink<String> ccInputSink;
  final Sink<String> expInputSink;
  final Sink<String> cvvInputSink;
  final Sink<String> nameInputSink;
  final MaskedTextController ccMask;
  final MaskedTextController expMask;

  const FillEntries({
    Key key,
    @required this.ccMask,
    @required this.expMask,
    @required this.ccInputSink,
    @required this.expInputSink,
    @required this.cvvInputSink,
    @required this.nameInputSink,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            controller: ccMask,
            keyboardType: TextInputType.number,
            maxLength: 19,
            style: TextStyle(color: Colors.black),
            onChanged: (out) => ccInputSink.add(ccMask.text),
            decoration: InputDecoration(
              labelText: "Credit Card Number",
              labelStyle: TextStyle(fontWeight: FontWeight.bold),
              border: OutlineInputBorder(),
            ),
          ),
          TextField(
            controller: expMask,
            keyboardType: TextInputType.number,
            maxLength: 5,
            style: TextStyle(color: Colors.black),
            onChanged: (out) => expInputSink.add(expMask.text),
            decoration: InputDecoration(
              labelStyle: TextStyle(fontWeight: FontWeight.bold),
              labelText: "MM/YY",
              border: OutlineInputBorder(),
            ),
          ),
          TextField(
            keyboardType: TextInputType.number,
            maxLength: 3,
            style: TextStyle(color: Colors.black),
            onChanged: (out) => cvvInputSink.add(out),
            decoration: InputDecoration(
              labelStyle: TextStyle(fontWeight: FontWeight.bold),
              labelText: "CVV",
              border: OutlineInputBorder(),
            ),
          ),
          TextField(
            keyboardType: TextInputType.text,
            maxLength: 20,
            style: TextStyle(color: Colors.black),
            onChanged: (out) => nameInputSink.add(out),
            decoration: InputDecoration(
              labelStyle: TextStyle(fontWeight: FontWeight.bold),
              labelText: "Name on card",
              border: OutlineInputBorder(),
            ),
          ),
        ],
      ),
    );
  }
}
