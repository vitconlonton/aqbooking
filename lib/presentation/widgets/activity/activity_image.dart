import 'package:flutter/material.dart';

class ActivityImage extends StatelessWidget {
  final String uri;

  const ActivityImage({Key key, this.uri}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: Image(width: 110.0, image: NetworkImage(uri), fit: BoxFit.cover),
    );
  }
}
