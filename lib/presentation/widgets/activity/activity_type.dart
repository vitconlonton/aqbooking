import 'package:flutter/material.dart';

class ActivityType extends Text {
  final _textStyle = TextStyle(color: Colors.grey);

  ActivityType(String data) : super(data);

  @override
  TextStyle get style => _textStyle;
}
