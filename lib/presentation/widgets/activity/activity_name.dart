import 'package:flutter/material.dart';

class ActivityName extends Text {
  final _textStyle = TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600);

  ActivityName(String data) : super(data);

  @override
  TextStyle get style => _textStyle;

  @override
  TextOverflow get overflow => TextOverflow.ellipsis;

  @override
  int get maxLines => 2;
}
