import 'package:flutter/material.dart';

import 'package:aqbooking/domain/entities/index.dart';
import 'index.dart';

class ActivityItem extends StatelessWidget {
  final Activity activity;
  final Function(Activity) onTap;

  ActivityItem({Key key, this.activity, this.onTap})
      : assert(activity != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final name = Container(width: 120.0, child: ActivityName(activity.name));

    return GestureDetector(
      onTap: () => onTap(activity) ?? null,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(40.0, 5.0, 20.0, 5.0),
            height: 170.0,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Padding(
              padding: EdgeInsets.fromLTRB(100.0, 20.0, 20.0, 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      name,
                      Column(
                        children: [
                          ActivityPrice('\$${activity.price}'),
                          Text('per pax', style: TextStyle(color: Colors.grey)),
                        ],
                      ),
                    ],
                  ),
                  ActivityType(activity.type),
                  ActivityRating(rating: activity.rating),
                  SizedBox(height: 10.0),
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.all(5.0),
                        width: 70.0,
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          activity.startTimes[0],
                        ),
                      ),
                      SizedBox(width: 10.0),
                      Container(
                        padding: EdgeInsets.all(5.0),
                        width: 70.0,
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        alignment: Alignment.center,
                        child: Text(activity.startTimes[1]),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Positioned(
            left: 20.0,
            top: 15.0,
            bottom: 15.0,
            child: ActivityImage(uri: activity.imageUrl),
          ),
        ],
      ),
    );
  }
}
