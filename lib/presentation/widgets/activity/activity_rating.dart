import 'package:flutter/material.dart';

class ActivityRating extends StatelessWidget {
  final int rating;

  ActivityRating({Key key, this.rating = 0})
      : assert(rating != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    String stars = '';

    for (int i = 0; i < rating; i++) {
      stars += '⭐ ';
    }

    stars.trim();

    return Text(stars);
  }
}
