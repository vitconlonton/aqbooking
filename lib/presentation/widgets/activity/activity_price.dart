import 'package:flutter/material.dart';

class ActivityPrice extends Text {
  final _textStyle = TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600);

  ActivityPrice(String data) : super(data);

  @override
  TextStyle get style => _textStyle;
}
