import 'package:flutter/material.dart';

class ProfileTile extends StatelessWidget {
  final title;
  final subtitle;
  final textColor;

  const ProfileTile({
    Key key,
    this.title,
    this.subtitle,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w700, color: textColor),
        ),
        SizedBox(height: 5.0),
        Text(
          subtitle,
          style: TextStyle(
              fontSize: 12.0, fontWeight: FontWeight.normal, color: textColor),
        ),
      ],
    );
  }
}
