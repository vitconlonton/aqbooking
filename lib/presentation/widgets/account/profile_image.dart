import 'package:flutter/material.dart';

class ProfileImage extends StatelessWidget {
  final uri;

  const ProfileImage({Key key, this.uri}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      decoration:
          BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(40.0))),
      child: CircleAvatar(
        backgroundImage: NetworkImage(uri),
        foregroundColor: Colors.black,
        radius: 30.0,
      ),
    );
  }
}
