import 'package:flutter/material.dart';

class DestinationImage extends StatelessWidget {
  final String uri;
  final double borderRadius;
  final double height;
  final double width;

  const DestinationImage({
    Key key,
    @required this.uri,
    this.borderRadius = 20.0,
    this.height = 180.0,
    this.width = 180.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(borderRadius),
      child: Image(
        height: height,
        width: width,
        image: NetworkImage(uri),
        fit: BoxFit.cover,
      ),
    );
  }
}
