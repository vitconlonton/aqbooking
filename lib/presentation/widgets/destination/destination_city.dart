import 'package:flutter/material.dart';

class DestinationCity extends Text {
  static const _textStyle = TextStyle(
    color: Colors.white,
    fontSize: 24.0,
    fontWeight: FontWeight.w600,
    letterSpacing: 1.2,
  );

  final textStyle;

  DestinationCity(String data, {this.textStyle}) : super(data);

  @override
  TextStyle get style => _textStyle.merge(textStyle);
}
