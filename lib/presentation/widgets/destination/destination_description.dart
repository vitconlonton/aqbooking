import 'package:flutter/material.dart';

class DestinationDescription extends Text {
  final _textStyle = TextStyle(color: Colors.grey);

  final textStyle;

  DestinationDescription(String data, {this.textStyle}) : super(data);

  @override
  TextStyle get style => _textStyle.merge(textStyle);

  @override
  int get maxLines => 2;
}
