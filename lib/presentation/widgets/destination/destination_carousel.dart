import 'package:flutter/material.dart';

import 'package:aqbooking/domain/entities/index.dart';
import 'index.dart';

final title = Text(
  'Top Destinations',
  style: TextStyle(
    fontSize: 22.0,
    fontWeight: FontWeight.bold,
    letterSpacing: 1.5,
  ),
);

class DestinationCarousel extends StatelessWidget {
  final List<Destination> destinations;
  final Function(Destination) onItemTap;
  final Function onSeeAllTap;

  const DestinationCarousel({
    Key key,
    this.destinations = const [],
    this.onItemTap,
    this.onSeeAllTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              title,
              GestureDetector(
                onTap: onSeeAllTap,
                child: Text(
                  'See All',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 16.0,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 1.0,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 300.0,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: destinations.length,
            itemBuilder: (BuildContext context, int index) {
              return DestinationItem(
                destination: destinations[index],
                onTap: () => onItemTap(destinations[index]),
              );
            },
          ),
        ),
      ],
    );
  }
}
