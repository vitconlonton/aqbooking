import 'package:flutter/material.dart';

class DestinationCountry extends Text {
  static const _textStyle = TextStyle(color: Colors.white70);

  final textStyle;

  DestinationCountry(String data, {this.textStyle}) : super(data);

  @override
  TextStyle get style => _textStyle.merge(textStyle);
}
