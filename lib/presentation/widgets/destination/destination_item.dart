import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:aqbooking/domain/entities/index.dart';
import 'index.dart';

final Widget _iconLocation = Icon(
  FontAwesomeIcons.locationArrow,
  size: 10.0,
  color: Colors.white,
);

final BoxDecoration _boxShadow = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(20.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black26,
      offset: Offset(0.0, 2.0),
      blurRadius: 6.0,
    ),
  ],
);

class DestinationItem extends StatelessWidget {
  final Destination destination;
  final Function onTap;

  DestinationItem({
    Key key,
    this.destination,
    this.onTap,
  })  : assert(destination != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    // Image
    Widget imageHero = Hero(
      tag: destination.imageUrl,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: Image(
          height: 180.0,
          width: 180.0,
          image: NetworkImage(destination.imageUrl),
          fit: BoxFit.cover,
        ),
      ),
    );

    // City, country
    Widget cityAndCountry = Positioned(
      left: 10.0,
      bottom: 10.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DestinationCity(destination.city ?? ""),
          Row(
            children: [
              _iconLocation,
              SizedBox(width: 5.0),
              DestinationCountry(destination.country ?? ""),
            ],
          ),
        ],
      ),
    );

    // City, country
    Widget activitiesContainer = Positioned(
      bottom: 15.0,
      child: Container(
        height: 120.0,
        width: 200.0,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ActivitiesNumber('${destination.activitiesNumber} activities'),
              DestinationDescription(destination.description)
            ],
          ),
        ),
      ),
    );

    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.all(10.0),
        width: 210.0,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            activitiesContainer,
            Container(
              decoration: _boxShadow,
              child: Stack(children: [imageHero, cityAndCountry]),
            )
          ],
        ),
      ),
    );
  }
}

class ActivitiesNumber extends Text {
  final _textStyle = TextStyle(
    fontSize: 22.0,
    fontWeight: FontWeight.w600,
    letterSpacing: 1.2,
  );

  ActivitiesNumber(String data) : super(data);

  @override
  TextStyle get style => _textStyle;
}
