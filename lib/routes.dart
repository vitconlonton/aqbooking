import 'package:flutter/material.dart';

import 'presentation/pages/auth/sign_in_page.dart';
import 'presentation/pages/auth/sign_up_page.dart';
import 'presentation/pages/auth/splash_page.dart';
import 'presentation/pages/home/home_page.dart';
import 'presentation/pages/payment/credit_card_page.dart';
import 'presentation/pages/payment/payment_success_page.dart';

class Routes {
  Routes._();

  //static variables
  static const String splash = '/splash';
  static const String sign_in = '/sign_in';
  static const String sign_up = '/sign_up';
  static const String home = '/home';
  static const String payment = '/payment';
  static const String payment_success = '/payment_success';
  static const String account_profile = '/account_profile';

  static final routes = <String, WidgetBuilder>{
    splash: (BuildContext context) => SplashPage(),
    sign_in: (BuildContext context) => SignInPage(),
    sign_up: (BuildContext context) => SignUpPage(),
    payment: (BuildContext context) => CreditCardPage(),
    payment_success: (BuildContext context) => PaymentSuccessPage(),
    home: (BuildContext context) => HomePage(),
  };
}
