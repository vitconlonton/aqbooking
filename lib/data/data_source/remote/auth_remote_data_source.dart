import 'package:flutter/foundation.dart';

import 'package:aqbooking/data/models/index.dart';
import 'package:aqbooking/core/network/index.dart';

/// End points Auth
const String BASE_URL = 'https://reqres.in/api/';
const String SIGN_IN_URL = BASE_URL + 'login';
const String SIGN_UP_URL = BASE_URL + 'register';
const String SIGN_OUT_URL = BASE_URL + 'logout';

abstract class AuthRemoteDataSource {
  /// Calls the {sign_in_url} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<SignInModel> signIn(
      {@required String email, @required String password});

  /// Calls the {sign_up_url} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<SignOutModel> signUp({
    @required String email,
    @required String phone,
    @required String password,
    @required String cPassword,
  });

  /// Calls the {sign_out_url} endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<bool> signOut();
}

class IAuthRemoteDataSource implements AuthRemoteDataSource {
  final DioClient dioClient;

  IAuthRemoteDataSource({@required this.dioClient});

  @override
  Future<SignInModel> signIn({
    @required String email,
    @required String password,
  }) async {
    var data = {"email": email, "password": password};

    return await dioClient
        .post(SIGN_IN_URL, data: data)
        .then((data) => SignInModel.fromJson(data))
        .catchError((e) => throw e);
  }

  @override
  Future<SignOutModel> signUp({
    String email,
    String phone,
    String password,
    String cPassword,
  }) async {
    var data = {"email": email, "password": password};

    return await dioClient
        .post(SIGN_UP_URL, data: data)
        .then((data) => SignOutModel.fromJson(data))
        .catchError((e) => throw e);
  }

  @override
  Future<bool> signOut() => Future.value(true);
}
