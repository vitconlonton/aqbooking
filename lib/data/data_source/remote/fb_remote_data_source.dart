import 'dart:convert';

import 'package:aqbooking/core/network/index.dart';
import 'package:aqbooking/data/exception/exceptions.dart';
import 'package:aqbooking/data/models/index.dart';
import 'package:flutter/foundation.dart';

/// Facebook end points
const String BASE_URL = 'https://graph.facebook.com/';
const String GET_PROFILE_URL =
    BASE_URL + 'v2.12/me?fields=id,name,picture{url}&access_token=';

abstract class FBRemoteDataSource {
  Future<FBUserProfile> getFBProfile({
    @required String userId,
    @required String token,
  });
}

class IFBRemoteDataSource implements FBRemoteDataSource {
  final DioClient dioClient;

  IFBRemoteDataSource({@required this.dioClient});

  @override
  Future<FBUserProfile> getFBProfile({String userId, String token}) {
    final url = GET_PROFILE_URL + token;

    return dioClient
        .get(url)
        .then((value) => jsonDecode(value))
        .then((json) => FBUserProfile.fromJson(json))
        .catchError((e) => throw FBException());
  }
}
