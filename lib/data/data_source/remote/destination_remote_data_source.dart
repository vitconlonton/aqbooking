import 'package:aqbooking/core/network/index.dart';
import 'package:aqbooking/data/models/index.dart';
import 'package:meta/meta.dart';

String image =
    'https://gigigriffis.com/wp-content/uploads/2015/03/2987495994_2b38ab1d00_o.jpg';

List<DestinationModel> destinations = [
  DestinationModel(
    imageUrl: image,
    city: 'Venice',
    country: 'Italy',
    description: 'Visit Venice for an amazing and unforgettable adventure.',
    activitiesNumber: 124,
  ),
  DestinationModel(
    imageUrl:
        'https://en.parisinfo.com/var/otcp/sites/images/node_43/node_51/node_230/vue-a%C3%A9rienne-paris-tour-eiffel-coucher-de-soleil-%7C-630x405-%7C-%C2%A9-fotolia/19544352-1-fre-FR/Vue-a%C3%A9rienne-Paris-Tour-Eiffel-coucher-de-soleil-%7C-630x405-%7C-%C2%A9-Fotolia.jpg',
    city: 'Paris',
    country: 'France',
    description: 'Visit Paris for an amazing and unforgettable adventure.',
    activitiesNumber: 13,
  ),
  DestinationModel(
    imageUrl: 'https://www.uas.aero/wp-content/uploads/2019/05/New-Delhi.jpg',
    city: 'New Delhi',
    country: 'India',
    description: 'Visit New Delhi for an amazing and unforgettable adventure.',
    activitiesNumber: 73,
  ),
  DestinationModel(
    imageUrl:
        'https://www.thegef.org/sites/default/files/shutterstock_450913045.jpg',
    city: 'Sao Paulo',
    country: 'Brazil',
    description: 'Visit Sao Paulo for an amazing and unforgettable adventure.',
    activitiesNumber: 56,
  ),
  DestinationModel(
    imageUrl:
        'https://blog-www.pods.com/wp-content/uploads/2019/04/MG_1_1_New_York_City-1.jpg',
    city: 'New York City',
    country: 'United States',
    description: 'Visit New York for an amazing and unforgettable adventure.',
    activitiesNumber: 500,
  ),
];

abstract class DestinationRemoteDataSource {
  /// Remove the auth token
  ///
  /// Throws [CacheException] if no cached data is present.
  Future<List<DestinationModel>> getTopDestinations();
}

class IDestinationRemoteDataSource implements DestinationRemoteDataSource {
  final DioClient dioClient;

  IDestinationRemoteDataSource({@required this.dioClient});

  @override
  Future<List<DestinationModel>> getTopDestinations() async {
    return Future.delayed(
      Duration(milliseconds: 700),
    ).then(
      (value) => destinations,
    );
  }
}
