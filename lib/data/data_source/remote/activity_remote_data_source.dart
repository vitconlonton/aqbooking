import 'package:meta/meta.dart';

import 'package:aqbooking/core/network/index.dart';
import 'package:aqbooking/data/models/index.dart';

String image =
    'https://prospect.org/downloads/13738/download/Engel%20032020%20Venice.jpg?cb=de91a1d463b7e281ecf75756938c7582&w=1024';

List<ActivityModel> activities = [
  ActivityModel(
    imageUrl: image,
    name: 'St. Mark\'s Basilica',
    type: 'Sightseeing Tour',
    startTimes: ['9:00 am', '11:00 am'],
    rating: 5,
    price: 30,
  ),
  ActivityModel(
    imageUrl: image,
    name: 'Walking Tour and Gondola Ride',
    type: 'Sightseeing Tour',
    startTimes: ['11:00 pm', '1:00 pm'],
    rating: 4,
    price: 210,
  ),
  ActivityModel(
    imageUrl: image,
    name: 'Maranon and Buran Tour',
    type: 'Sightseeing Tour',
    startTimes: ['12:30 pm', '2:00 pm'],
    rating: 3,
    price: 125,
  ),
];

abstract class ActivityRemoteDataSource {
  /// Remove the auth token
  ///
  /// Throws [CacheException] if no cached data is present.
  Future<List<ActivityModel>> getActivitiesByDestinationId({
    @required int desId,
  });
}

class IActivityRemoteDataSource implements ActivityRemoteDataSource {
  final DioClient dioClient;

  IActivityRemoteDataSource({@required this.dioClient});

  @override
  Future<List<ActivityModel>> getActivitiesByDestinationId({int desId}) {
    return Future.delayed(
      Duration(milliseconds: 700),
    ).then((value) => activities);
  }
}
