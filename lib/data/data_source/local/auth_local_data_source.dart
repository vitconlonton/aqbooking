import 'package:aqbooking/data/exception/exceptions.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class AuthLocalDataSource {
  /// Remove the auth token
  ///
  /// Throws [CacheException] if no cached data is present.
  Future<String> getAuthToken();

  /// Cache the auth token
  ///
  /// Return [True] if cached success, else return [False].
  Future<bool> updateAuthToken(String token);

  /// Remove the auth token
  ///
  /// Return [True] if remove token success, else return [False].
  Future<bool> removeAuthToken();
}

const String AUTH_TOKEN_KEY = "AUTH_TOKEN";

class IAuthLocalDataSource implements AuthLocalDataSource {
  final SharedPreferences sharedPreferences;

  IAuthLocalDataSource({@required this.sharedPreferences});

  @override
  Future<String> getAuthToken() {
    // Get token from local data source
    final token = sharedPreferences.getString(AUTH_TOKEN_KEY);

    return Future.value(token);
  }

  @override
  Future<bool> updateAuthToken(String token) =>
      sharedPreferences.setString(AUTH_TOKEN_KEY, token);

  @override
  Future<bool> removeAuthToken() => sharedPreferences.remove(AUTH_TOKEN_KEY);
}
