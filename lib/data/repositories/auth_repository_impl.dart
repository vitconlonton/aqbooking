import 'package:aqbooking/core/network/index.dart';
import 'package:aqbooking/data/data_source/local/auth_local_data_source.dart';
import 'package:aqbooking/data/data_source/remote/auth_remote_data_source.dart';
import 'package:aqbooking/data/exception/exceptions.dart';
import 'package:aqbooking/domain/entities/credential.dart';
import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/domain/error/failures.dart';
import 'package:aqbooking/domain/repositories/index.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

const String AQBOOKING_SERVICE = "AQ";

class IAuthRepository implements AuthRepository {
  final AuthLocalDataSource localDataSource;
  final AuthRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  IAuthRepository({
    @required this.remoteDataSource,
    @required this.networkInfo,
    @required this.localDataSource,
  });

  @override
  Future<Either<bool, Failure>> isLoggedIn() async {
    String token = await localDataSource.getAuthToken();

    return Left(token != null);
  }

  @override
  Future<Either<Credential, Failure>> signIn(
      {String email, String password}) async {
    try {
      // Request api sign in
      final remoteAuth = await remoteDataSource.signIn(
        email: email,
        password: password,
      );

      // If sign in success store token into local data source
      await localDataSource
          .updateAuthToken(remoteAuth.authToken)
          .then((value) => {if (!value) throw CacheException()});

      final credential = Credential(
        userId: "",
        accessToken: remoteAuth.authToken,
        service: ServiceType.aq,
      );

      return Left(credential);
    } on ServerException {
      return Right(ServerFailure());
    } on CacheException {
      return Right(CacheFailure());
    }
  }

  @override
  Future<Either<Credential, Failure>> signUp({
    String email,
    String phone,
    String password,
    String cPassword,
  }) async {
    try {
      // Request api sign up
      final remoteAuth = await remoteDataSource.signUp(
        email: email,
        phone: phone,
        password: password,
        cPassword: cPassword,
      );

      // If sign up success store token into local data source
      await localDataSource.updateAuthToken(remoteAuth.authToken);

      final credential = Credential(
        userId: "",
        accessToken: remoteAuth.authToken,
        service: ServiceType.aq,
      );

      return Left(credential);
    } on ServerException {
      return Right(ServerFailure());
    } on CacheException {
      return Right(CacheFailure());
    }
  }

  @override
  Future<Either<bool, Failure>> signOut() async {
    try {
      await Future.delayed(Duration(seconds: 1));

      // Request api sign out
      await remoteDataSource
          .signOut()
          .then((value) => {if (!value) throw ServerException()});

      // If sign out success store token into local data source
      await localDataSource
          .removeAuthToken()
          .then((value) => {if (!value) throw CacheException()});

      return Left(true);
    } on ServerException {
      return Right(ServerFailure());
    } on CacheException {
      return Right(CacheFailure());
    }
  }
}
