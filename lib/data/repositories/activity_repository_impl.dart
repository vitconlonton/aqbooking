import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/domain/error/failures.dart';
import 'package:aqbooking/domain/repositories/index.dart';
import 'package:aqbooking/data/data_source/remote/activity_remote_data_source.dart';

class IActivityRepository implements ActivityRepository {
  final ActivityRemoteDataSource remoteDataSource;

  IActivityRepository({@required this.remoteDataSource});

  @override
  Future<Either<List<Activity>, Failure>> getActivitiesByDesId({
    int desId,
  }) async {
    return await remoteDataSource
        .getActivitiesByDestinationId(desId: desId)
        .catchError((e) => Right(ServerFailure()))
        .then((activitiesModel) => activitiesModel
            .map(
              (model) => Activity(
                imageUrl: model.imageUrl,
                name: model.name,
                type: model.type,
                startTimes: model.startTimes,
                rating: model.rating,
                price: model.price,
              ),
            )
            .toList())
        .then((activities) => Left(activities));
  }
}
