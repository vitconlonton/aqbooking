import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import 'package:aqbooking/domain/entities/account.dart';
import 'package:aqbooking/domain/error/failures.dart';
import 'package:aqbooking/domain/repositories/index.dart';
import 'package:aqbooking/data/data_source/remote/fb_remote_data_source.dart';

class IFBRepository implements FBRepository {
  final FBRemoteDataSource remoteDataSource;

  IFBRepository({@required this.remoteDataSource});

  @override
  Future<Either<Account, Failure>> getFBUserProfile({
    String userId,
    String token,
  }) async {
    return await remoteDataSource
        .getFBProfile(userId: userId, token: token)
        .catchError((error) => Right(FBFailure()))
        .then(
          (profile) => Left(
            Account(
              id: profile.id,
              name: profile.name,
              picture: profile.imgUrl,
            ),
          ),
        );
  }
}
