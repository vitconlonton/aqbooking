import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/domain/error/failures.dart';
import 'package:aqbooking/domain/repositories/index.dart';
import 'package:aqbooking/data/data_source/remote/destination_remote_data_source.dart';
import 'package:aqbooking/data/exception/exceptions.dart';

class IDestinationRepository implements DestinationRepository {
  final DestinationRemoteDataSource remoteDataSource;

  IDestinationRepository({@required this.remoteDataSource});

  @override
  Future<Either<List<Destination>, Failure>> getTopDestinations() async {
    return await remoteDataSource
        .getTopDestinations()
        .catchError((error) => Right(ServerException()))
        .then((destinationsModel) => destinationsModel
            .map(
              (model) => Destination(
                imageUrl: model.imageUrl,
                city: model.city,
                country: model.country,
                description: model.description,
                activitiesNumber: model.activitiesNumber,
              ),
            )
            .toList())
        .then((destinations) => Left(destinations));
  }
}
