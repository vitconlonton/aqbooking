import 'package:aqbooking/data/models/base_model.dart';

class FBUserProfile extends BaseModel {
  final String id;
  final String name;
  final String imgUrl;

  FBUserProfile({this.id, this.name, this.imgUrl});

  factory FBUserProfile.fromJson(Map<String, dynamic> json) {
    return FBUserProfile(
      id: json['id'].toString(),
      name: json['name'],
      imgUrl: json['picture']["data"]["url"],
    );
  }
}
