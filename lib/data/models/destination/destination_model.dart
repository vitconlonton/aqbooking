class DestinationModel {
  final String imageUrl;
  final String city;
  final String country;
  final String description;
  final int activitiesNumber;

  DestinationModel({
    this.imageUrl,
    this.city,
    this.country,
    this.description,
    this.activitiesNumber,
  });
}
