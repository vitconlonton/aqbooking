import 'package:meta/meta.dart';

class SignInModel {
  final String authToken;

  SignInModel({@required this.authToken});

  factory SignInModel.fromJson(Map<String, dynamic> json) {
    return SignInModel(authToken: json['token']);
  }

  Map<String, dynamic> toJson() => {'authToken': authToken};
}
