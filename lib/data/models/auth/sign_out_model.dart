import 'package:meta/meta.dart';

class SignOutModel {
  final int id;
  final String authToken;

  SignOutModel({@required this.authToken, @required this.id});

  factory SignOutModel.fromJson(Map<String, dynamic> json) {
    return SignOutModel(id: json['id'], authToken: json['token']);
  }

  Map<String, dynamic> toJson() => {'authToken': authToken, 'id': id};
}
