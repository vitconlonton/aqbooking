import 'dart:math';

import 'package:flutter/material.dart';

class UIData {
  //generic
  static const String error = "Error";
  static const String success = "Success";
  static const String ok = "OK";
  static const String forgot_password = "Forgot Password?";
  static const String something_went_wrong = "Something went wrong";
  static const String coming_soon = "Coming Soon";

  static const MaterialColor ui_kit_color = Colors.grey;

//colors
  static List<Color> kitGradients = [Colors.blueGrey.shade800, Colors.black87];
  static List<Color> kitGradients2 = [
    Colors.cyan.shade600,
    Colors.blue.shade900
  ];

  //random color
  static final Random _random = new Random();

  /// Returns a random color.
  static Color next() => new Color(0xFF000000 + _random.nextInt(0x00FFFFFF));
}
