abstract class NetworkInfo {
  Future<bool> get isConnected;
}

class INetworkInfo implements NetworkInfo {
  INetworkInfo();

  @override
  Future<bool> get isConnected =>
      Future.delayed(Duration(milliseconds: 200)).then((value) => true);
}
