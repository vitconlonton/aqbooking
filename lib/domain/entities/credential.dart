import 'package:flutter/foundation.dart';

import 'base_entity.dart';

enum ServiceType { aq, fb }

class Credential extends BaseEntity {
  final String userId;
  final ServiceType service;
  final String tokenType;
  final String accessToken;
  final String refreshToken;
  final int expiresIn;

  Credential({
    @required this.userId,
    @required this.service,
    @required this.accessToken,
    this.tokenType,
    this.expiresIn,
    this.refreshToken,
  });

  @override
  List<Object> get props => [
        userId,
        service,
        tokenType,
        accessToken,
        refreshToken,
        expiresIn,
      ];
}
