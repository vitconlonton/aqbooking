import 'base_entity.dart';

class Activity extends BaseEntity {
  final String imageUrl;
  final String name;
  final String type;
  final List<String> startTimes;
  final int rating;
  final int price;

  Activity({
    this.imageUrl,
    this.name,
    this.type,
    this.startTimes,
    this.rating,
    this.price,
  });

  @override
  List<Object> get props => [imageUrl, name, type, startTimes, rating, price];
}
