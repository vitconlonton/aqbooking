import 'base_entity.dart';

class Destination extends BaseEntity {
  final String imageUrl;
  final String city;
  final String country;
  final String description;
  final int activitiesNumber;

  Destination({
    this.imageUrl,
    this.city,
    this.country,
    this.description,
    this.activitiesNumber,
  });

  @override
  List<Object> get props => [
        imageUrl,
        city,
        country,
        description,
        activitiesNumber,
      ];
}
