import 'base_entity.dart';

class Account extends BaseEntity {
  final String id;
  final String name;
  final String picture;

  Account({this.id, this.name, this.picture});

  @override
  List<Object> get props => [id, name, picture];
}
