import 'package:aqbooking/domain/entities/index.dart';
import 'package:dartz/dartz.dart';

import 'package:aqbooking/domain/error/failures.dart';
import 'package:aqbooking/domain/repositories/index.dart';
import 'package:aqbooking/domain/use_cases/use_case.dart';

class GetTopDestinations implements UseCase<List<Destination>, NoParams> {
  final DestinationRepository repository;

  GetTopDestinations(this.repository);

  @override
  Future<Either<List<Destination>, Failure>> call(NoParams params) {
    return repository.getTopDestinations();
  }
}
