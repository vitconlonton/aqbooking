import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/domain/error/failures.dart';
import 'package:aqbooking/domain/repositories/index.dart';
import 'package:aqbooking/domain/use_cases/use_case.dart';

class GetActivitiesByDesId
    implements UseCase<List<Activity>, GetActivitiesByDesIdParams> {
  final ActivityRepository repository;

  GetActivitiesByDesId(this.repository);

  @override
  Future<Either<List<Activity>, Failure>> call(
    GetActivitiesByDesIdParams params,
  ) {
    return repository.getActivitiesByDesId(desId: params.desId);
  }
}

class GetActivitiesByDesIdParams extends Equatable {
  final int desId;

  GetActivitiesByDesIdParams({@required this.desId});

  @override
  List<Object> get props => [desId];
}
