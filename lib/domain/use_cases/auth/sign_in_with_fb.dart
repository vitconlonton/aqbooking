import 'package:dartz/dartz.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

import 'package:aqbooking/domain/entities/credential.dart';
import 'package:aqbooking/domain/error/failures.dart';
import 'package:aqbooking/domain/use_cases/use_case.dart';

class SignInWithFB implements UseCase<Credential, NoParams> {
  final _facebookLogin = FacebookLogin();

  SignInWithFB();

  @override
  Future<Either<Credential, Failure>> call(NoParams params) {
    return _facebookLogin.logIn(["email"]).then((result) {
      final status = result.status;

      if (status != FacebookLoginStatus.loggedIn) {
        return Right(ServerFailure());
      }

      final credential = Credential(
        userId: result.accessToken.userId,
        accessToken: result.accessToken.token,
        service: ServiceType.fb,
      );

      return Left(credential);
    });
  }
}
