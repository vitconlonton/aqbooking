import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'package:aqbooking/domain/entities/credential.dart';
import 'package:aqbooking/domain/error/failures.dart';
import 'package:aqbooking/domain/repositories/index.dart';
import 'package:aqbooking/domain/use_cases/use_case.dart';

class SignUp implements UseCase<Credential, SignUpParams> {
  final AuthRepository repository;

  SignUp(this.repository);

  @override
  Future<Either<Credential, Failure>> call(SignUpParams params) {
    return repository.signUp(
      email: params.email,
      password: params.password,
      phone: params.phone,
      cPassword: params.cPassword,
    );
  }
}

class SignUpParams extends Equatable {
  final String email;
  final String phone;
  final String password;
  final String cPassword;

  SignUpParams({
    @required this.email,
    @required this.phone,
    @required this.password,
    @required this.cPassword,
  });

  @override
  List<Object> get props => [email, password];
}
