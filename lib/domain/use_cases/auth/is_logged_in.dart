import 'package:dartz/dartz.dart';

import 'package:aqbooking/domain/error/failures.dart';
import 'package:aqbooking/domain/repositories/index.dart';
import 'package:aqbooking/domain/use_cases/use_case.dart';

class IsLoggedIn implements UseCase<bool, NoParams> {
  final AuthRepository repository;

  IsLoggedIn(this.repository);

  @override
  Future<Either<bool, Failure>> call(NoParams noParams) =>
      repository.isLoggedIn();
}
