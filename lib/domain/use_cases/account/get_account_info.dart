import 'package:dartz/dartz.dart';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'package:aqbooking/domain/repositories/index.dart';
import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/domain/error/failures.dart';
import 'package:aqbooking/domain/use_cases/use_case.dart';

class GetAccountInfo implements UseCase<Account, GetAccountParams> {
  final FBRepository repository;

  GetAccountInfo(this.repository);

  @override
  Future<Either<Account, Failure>> call(GetAccountParams params) {
    return repository.getFBUserProfile(
      userId: params.userId,
      token: params.token,
    );
  }
}

class GetAccountParams extends Equatable {
  final String userId;
  final String token;

  GetAccountParams({@required this.userId, @required this.token});

  @override
  List<Object> get props => [userId, token];
}
