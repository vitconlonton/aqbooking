import 'package:flutter/foundation.dart';
import 'package:dartz/dartz.dart';

import 'package:aqbooking/domain/entities/index.dart';
import 'package:aqbooking/domain/error/failures.dart';

abstract class ActivityRepository {
  Future<Either<List<Activity>, Failure>> getActivitiesByDesId({
    @required int desId,
  });
}
