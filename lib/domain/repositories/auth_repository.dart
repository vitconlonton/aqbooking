import 'package:aqbooking/domain/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';

import 'package:aqbooking/domain/entities/credential.dart';

abstract class AuthRepository {
  Future<Either<Credential, Failure>> signIn({
    @required String email,
    @required String password,
  });

  Future<Either<Credential, Failure>> signUp({
    @required String email,
    @required String phone,
    @required String password,
    @required String cPassword,
  });

  Future<Either<bool, Failure>> isLoggedIn();

  Future<Either<bool, Failure>> signOut();
}
