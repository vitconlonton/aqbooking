import 'package:flutter/foundation.dart';
import 'package:dartz/dartz.dart';

import 'package:aqbooking/domain/entities/account.dart';
import 'package:aqbooking/domain/error/failures.dart';

abstract class FBRepository {
  Future<Either<Account, Failure>> getFBUserProfile({
    @required String userId,
    @required String token,
  });
}
