import 'package:aqbooking/domain/entities/index.dart';
import 'package:dartz/dartz.dart';

import 'package:aqbooking/domain/error/failures.dart';

abstract class DestinationRepository {
  Future<Either<List<Destination>, Failure>> getTopDestinations();
}
