import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/constants/app_theme.dart';
import 'di/injection_container.dart' as di;
import 'package:aqbooking/di/injection_container.dart';
import 'presentation/stores/app_bloc/app_bloc.dart';
import 'routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      bloc: sl<AppBloc>(),
      builder: (context, state) => MaterialApp(
        routes: Routes.routes,
        debugShowCheckedModeBanner: false,
        theme: themeData,
        initialRoute: Routes.sign_in,
      ),
    );
  }
}
