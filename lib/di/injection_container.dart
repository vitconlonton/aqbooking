import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:aqbooking/core/network/index.dart';
import 'package:aqbooking/data/data_source/local/auth_local_data_source.dart';
import 'package:aqbooking/data/data_source/remote/activity_remote_data_source.dart';
import 'package:aqbooking/data/data_source/remote/auth_remote_data_source.dart';
import 'package:aqbooking/data/data_source/remote/destination_remote_data_source.dart';
import 'package:aqbooking/data/data_source/remote/fb_remote_data_source.dart';
import 'package:aqbooking/data/repositories/activity_repository_impl.dart';
import 'package:aqbooking/data/repositories/auth_repository_impl.dart';
import 'package:aqbooking/data/repositories/destination_repository_impl.dart';
import 'package:aqbooking/data/repositories/fb_repository_impl.dart';
import 'package:aqbooking/domain/repositories/index.dart';
import 'package:aqbooking/domain/use_cases/account/index.dart';
import 'package:aqbooking/domain/use_cases/activity/index.dart';
import 'package:aqbooking/domain/use_cases/auth/index.dart';
import 'package:aqbooking/domain/use_cases/destination/index.dart';
import 'package:aqbooking/presentation/stores/app_bloc/app_bloc.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //! Features - Auth
  // Data sources
  sl.registerLazySingleton<AuthRemoteDataSource>(
    () => IAuthRemoteDataSource(dioClient: sl()),
  );

  sl.registerLazySingleton<AuthLocalDataSource>(
    () => IAuthLocalDataSource(sharedPreferences: sl()),
  );

  // Bloc
  // sl.registerFactory(() => AppBloc(getAccountInfo: sl(), signOut: sl()));
  // sl.registerFactory(() => SignUpBloc(signUp: sl()));

  // Repository
  sl.registerLazySingleton<AuthRepository>(
    () => IAuthRepository(
      localDataSource: sl(),
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );

  // Use case
  sl.registerLazySingleton(() => IsLoggedIn(sl()));
  sl.registerLazySingleton(() => SignIn(sl()));
  sl.registerLazySingleton(() => SignUp(sl()));
  sl.registerLazySingleton(() => SignInWithFB());
  sl.registerLazySingleton(() => SignOut(sl()));

  //! Features - Account
  // Data sources

  // Repository

  // Use case
  sl.registerLazySingleton(() => GetAccountInfo(sl()));

  //! Features - Facebook

  // Data sources
  sl.registerLazySingleton<FBRemoteDataSource>(
    () => IFBRemoteDataSource(dioClient: sl()),
  );

  // Repository
  sl.registerLazySingleton<FBRepository>(
    () => IFBRepository(
      remoteDataSource: sl(),
    ),
  );

  // Use case

  //! Features - Destination
  // Data sources
  sl.registerLazySingleton<DestinationRemoteDataSource>(
    () => IDestinationRemoteDataSource(dioClient: sl()),
  );

  // Repository
  sl.registerLazySingleton<DestinationRepository>(
    () => IDestinationRepository(remoteDataSource: sl()),
  );

  // Use case
  sl.registerLazySingleton(() => GetTopDestinations(sl()));

  //! Features - Activity
  // Data sources
  sl.registerLazySingleton<ActivityRemoteDataSource>(
    () => IActivityRemoteDataSource(dioClient: sl()),
  );

  // Repository
  sl.registerLazySingleton<ActivityRepository>(
    () => IActivityRepository(remoteDataSource: sl()),
  );

  // Use case
  sl.registerLazySingleton(() => GetActivitiesByDesId(sl()));

  //! Features - App
  // Bloc
  sl.registerLazySingleton<AppBloc>(() => AppBloc());

  //! Core
  sl.registerLazySingleton<NetworkInfo>(() => INetworkInfo());
  sl.registerLazySingleton<DioClient>(() => DioClient(Dio()));

  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
}
